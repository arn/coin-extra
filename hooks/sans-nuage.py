#!/usr/bin/python3
import json
import csv
import sys
from subprocess import Popen, PIPE
from email.mime.text import MIMEText
from pathlib import Path
import time
import random
import string
import os
import re

command = os.environ['SSH_ORIGINAL_COMMAND']
if command == 'states':
    answer = []

    # Get info from sans-nuage
    p = Popen(["sudo", "yunohost", "user", "list", "--output-as", "json"], stdout=PIPE)
    out, err = p.communicate()
    states = json.loads(out.decode('utf-8'))
    states = states['users']

    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        if row['login'] in states.keys():
            answer.append({
                'id': row['id'],
                'provisioned': 'yes',
                'status': 'actif',
                'status_color': 'green'
            })
            del states[row['login']]
        else:
            answer.append({
                'id': row['id'],
                'provisioned': 'no',
                'status': '',
                'status_color': 'red'
            })
    print(json.dumps(answer))

    if len(states.keys()) > 4:
        from pathlib import Path
        lastmail = Path('lastmail') 
        if not lastmail.exists() or (time.time() - lastmail.stat().st_mtime) / 60 / 60 / 24 > 1:
            msg = MIMEText("Il y a des comptes sans-nuage qui ne sont pas répercutés dans COIN: {usernames}".format(usernames=str(states.keys())))
            msg['Subject'] = 'Incohérence COIN/Sans-nuage'
            msg['From'] = 'root@sans-nuage.fr'
            msg['To'] = 'sans-nuage-support@arn-fai.net'

            p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
            p.communicate(msg.as_bytes())
            lastmail.touch()
elif command == 'create':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        login = row['login']
        mail = row['mail']
        firstname = row['firstname']
        lastname = row['lastname']
        regex_name = r'[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+'
        assert re.match(r'[a-z0-9-]{3,15}', login), "Login incorrect : {login}".format(login=login)
        assert re.match(regex_name, login), "Firstname : {firstname}".format(firstname=firstname)
        assert re.match(regex_name, login), "Lastname : {lastname}".format(lastname=lastname)
        letters = string.ascii_lowercase
        password = ''.join(random.choice(letters) for i in range(20))
        p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'create', '-f', firstname, '-l', lastname, '-m', login + '@sans-nuage.fr', '-q', '1G', '-p', password, login], stdout=PIPE)
        out, err = p.communicate()
        
        msg = MIMEText("Bonjour {firstname} {lastname},\n\nVotre compte sans-nuage.fr vient d'être créé :)\n\n Pour se connecter:\nIdentifiant: {login}\nMot de passe à changer: {password}\nInterface de connexion: https://sans-nuage.fr/fr/user.php\n\nPlus d'info et d'aide se trouve dans l'espace membre : https://adherents.arn-fai.net\n\nUne question ? -> support@sans-nuage.fr\n\nBonne journée,\nCeci est un message automatique".format(firstname=firstname, lastname=lastname, password=password, login=login))
        msg['Subject'] = 'Votre compte sans-nuage.fr est prêt :)'
        msg['From'] = 'support@sans-nuage.fr'
        msg['To'] = mail

        p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
        p.communicate(msg.as_bytes())
    print("{}")
elif command == 'remove':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        login = row['login']
        assert re.match(r'[a-z0-9-]{3,15}', login), "Login incorrect : {login}".format(login=login)

        p = Popen(["sudo", "/usr/bin/yunohost", 'user', 'delete', '--purge', login], stdout=PIPE)
        out, err = p.communicate()
    print("{}")
else:
    print('Unknown command: ' + command)
