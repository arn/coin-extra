#!/usr/bin/python3
import json
import csv
import sys
from subprocess import Popen, PIPE
from email.mime.text import MIMEText
from pathlib import Path
import time
import datetime
import os
import re
from OpenSSL import crypto

command = os.environ['SSH_ORIGINAL_COMMAND']
if command == 'states':
    answer = []

    states = {}
    p = Popen(["sudo", "gnt-instance", "list", "--separator=;","-o", "name,pnode,os,status,oper_ram,oper_vcpus,disk.size/0,disk.size/1,hv/boot_order,hv/root_path,network_port,custom_osparams,tags"], stdout=PIPE)
    out, err = p.communicate()
    out = out.decode('utf8')
    for row in csv.DictReader(out.splitlines(), delimiter=';'):
        #Instance;Primary_node;OS;Status;Memory;VCPUs;Disk/0;Disk/1;Boot_order;hv/root_path;Network_port;Tags;CustomOpSysParameters
        ips = json.loads(row['CustomOpSysParameters'].replace("'",'"'))
        if 'ipv4' in ips:
            row['ipv4'] = ips['ipv4']
            row['ipv6'] = ips['ipv6']
            states[ips['ipv4']] = row


    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        ipv4 = row['ipv4']
        if ipv4 in states.keys():
            # Instance status;
            # “running” if instance is set to be running and actually is,
            # “ADMIN_down” if instance is stopped and is not running,
            # “ERROR_wrongnode” if instance running, but not on its designated primary node,
            # “ERROR_up” if instance should be stopped, but is actually running,
            # “ERROR_down” if instance should run, but doesn’t,
            # “ERROR_nodedown” if instance’s primary node is down,
            # “ERROR_nodeoffline” if instance’s primary node is marked offline,
            # “ADMIN_offline” if instance is offline and does not use dynamic resources
            status = states[row['ipv4']]['Status']
            status_color = 'red'
            if status in ['running']:
                status_color = 'green'
            elif status in ['ERROR_up', 'ERROR_nodedown', 'ERROR_nodeoffline', 'ERROR_wrongnode']:
                status_color = 'orange'

            answer.append({
                'id': row['id'],
                'provisioned': 'yes',
                'status': status,
                'status_color': status_color,
            })
            del states[ipv4]
        else:
            answer.append({
                'id': row['id'],
                'provisioned': 'no',
                'status': 'N/A',
                'status_color': 'red'
            })
    print(json.dumps(answer))

    if len(states.keys()) > 0:
        from pathlib import Path
        lastmail = Path('lastmail')
        if not lastmail.exists() or (time.time() - lastmail.stat().st_mtime) / 60 / 60 / 24 > 1:
            msg = MIMEText("Il y a des comptes VPS qui ne sont pas répercutés dans COIN: {ips}".format(ips=str(states.keys())))
            msg['Subject'] = 'Incohérence COIN/VPS'
            msg['From'] = 'root@arn-fai.net'
            msg['To'] = 'infra-support@arn-fai.net'

            p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
            p.communicate(msg.as_bytes())
            lastmail.touch()

elif command == 'create':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        vps_id = 'vps-' + row['id']
        login = row['login']
        ipv4 = row['ipv4']
        ipv6 = row['ipv6']
        regex_ipv6 = r'(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))'

        assert re.match(r'(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' + r'/32', ipv4), "IPv4 incorrecte : {ipv4}".format(ipv4=ipv4)
        assert re.match(regex_ipv6 + r'/128', ipv6), "IPv6 incorrecte : {ipv6}".format(ipv6=ipv6)

        p = Popen(["sudo", "/usr/local/sbin/create-vm-arn", '-n', vps_id, '-d10', 'D200', '-r1024', '-4', ipv4, '-6', ipv6, '-s', os, '-k', key_path], stdout=PIPE)
        out, err = p.communicate()
    print("{}")
elif command == 'reboot':
    for row in csv.DictReader(iter(sys.stdin.readline, ''), delimiter=';'):
        vps_id = 'vps-' + row['id']
        assert re.match(r'\d+', row['id']), "ID incorrecte"

        p = Popen(["sudo", "gnt-instance", "reboot", vps_id], stdout=PIPE)
        out, err = p.communicate()
    print("{}")
elif command == 'reinstall':
    print("{}")
else:
    print('Unknown command: ' + command)
