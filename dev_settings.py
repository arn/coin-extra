ALLOWED_HOSTS = ['adherents.arn-fai.net', 'adh.arn-fai.net', 'coin.local']

URL_PREFIX = ''
SITE_URL = "https://coin.local/"
SECRET_KEY = 'OJ1kA7Q@TF=^U*uRKj&OCHaB3'
ISP = {
    'NAME': 'ARN',
    'SITE': 'https://arn-fai.net',
    'EMAIL': 'no@arn-fai.net',
}
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

FEEDS = (('isp', 'https://arn-fai.net/rss.xml', 3),
         ('ffdn', 'http://www.ffdn.org/fr/rss.xml', 3))


ACCOUNT_ACTIVATION_DAYS = 7

REGISTRATION_OPEN = True

MEMBER_DEFAULT_COTISATION = None

MEMBER_CAN_EDIT_PROFILE = True

EXTRA_INSTALLED_APPS = (
    'vpn',
    'vps',
    'housing',
    'hardware_provisioning',
)

# Allow user to edit their VPS Info
MEMBER_CAN_EDIT_VPS_CONF = False

# Allow user to edit their VPN Info
MEMBER_CAN_EDIT_VPN_CONF = False

LOGGING["formatters"]["verbose"] = {'format': "%(asctime)s - %(name)s - %(levelname)s - %(message)s"}
LOGGING["handlers"]["coin_accounting"] = {
    'level':'INFO',
    'class':'logging.handlers.RotatingFileHandler',
    'formatter': 'verbose',
    'filename': '/var/log/coin/accounting.log',
    'maxBytes': 1024*1024*15, # 15MB
    'backupCount': 10,
}
LOGGING["handlers"]["coin_subnets"] = {
    'level':'INFO',
    'class':'logging.handlers.RotatingFileHandler',
    'formatter': 'verbose',
    'filename': '/var/log/coin/subnet.log',
    'maxBytes': 1024*1024*15, # 15MB
    'backupCount': 10,
}
LOGGING["loggers"]["coin.billing"]["handlers"] = [ 'coin_accounting' ]
LOGGING["loggers"]["coin.subnets"]["handlers"] = [ 'coin_subnets' ]

