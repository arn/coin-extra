# coin-extra

Create a coin environnement with data from a coin_ynh setup

```
yunohost app install https://github.com/YunoHost-Apps/coin_ynh --debug
cd /vagrant
git clone https://code.ffdn.org/ARN/coin -b arnprod
git clone https://code.ffdn.org/ARN/coin-extra
cd coin-extra
chmod u+x coin-dev
scp adherents.arn-fai.net:~/dump.sql ../dump.sql
./coin-dev -g ../coin -r
```
